﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class functionRPC : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnDisconnectedFromServer(){
		SceneManager.LoadScene ("StartGame",LoadSceneMode.Single);
	}

	[RPC]
	public void RpcServer(string s){
		Debug.Log (s);
	}

	void OnConnectedToServer(){
		this.GetComponent<NetworkView> ().RPC ("sendInfoToServer",RPCMode.All,new object[]{Network.player.ToString(),Application.platform.ToString(),Screen.width,Screen.height});
	}

	[RPC]
	public void sendInfoToServer(string id,string os,int width,int height){
		
	}
}
