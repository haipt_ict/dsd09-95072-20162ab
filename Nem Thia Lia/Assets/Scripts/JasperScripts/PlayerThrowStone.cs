﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class PlayerThrowStone : MonoBehaviour
{

    public Text distanceText;
    public Text angleText;

    public float firingAngle = 10.0f;
    public float distance = 80f;
    public float gravity = 9.82f;

    public const float MAX_ANGLE = 90.0f;
    public const float MIN_ANGLE = 10.0f;
    public const float MAX_DISTANCE = 1000.0f;
    public const float MIN_DISTANCE = 10.0f;

    public const float DELTA = 5.0f;

    public string nameStone = "Stone One";

    public Transform Projectile;
    public StoneJumping BallScript;

    void Awake()
    {
    }

    void Start()
    {
        print("Stone Start !!");
        Projectile.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
        GameObject Ball = GameObject.Find(nameStone);
        BallScript = Ball.GetComponent<StoneJumping>();
        firingAngle = 10f;
        distance = 40f;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            StartCoroutine(SimulateProjectile(distance));
        }
    }


    IEnumerator SimulateProjectile(float dis)
    {
        yield return new WaitForSeconds(0.5f);

        Debug.Log("dhdjdhd" + firingAngle);

        Projectile.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);


        float stoneNew_Velocity = dis / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);
        print("Velocity Stone: " + stoneNew_Velocity);

        float Vz = Mathf.Sqrt(stoneNew_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
        float Vy = Mathf.Sqrt(stoneNew_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

        Debug.Log("Van toc Vz ban dau: " + Vz);

        BallScript.move(0, Vy, Vz);

    }
}
