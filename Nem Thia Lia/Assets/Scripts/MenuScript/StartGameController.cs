﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameController : MonoBehaviour {
    public string connectionIP = "127.0.0.1";
    public int portNumber = 8895;

    public void startGame()
    {
        Network.Connect(connectionIP, portNumber);
		SceneManager.LoadScene("MainGame");
    }
		

}
