﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneJumping : MonoBehaviour {

	public Rigidbody rb;
	public float gravity = 9.82f;
	public float pi = 3.14f;
	bool isFalling;
	float speedZ;
	float speedY;


	void Start () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (isFalling == false) {
			isFalling = true;
			Vector3 v = rb.velocity;
			v.y = speedY;
			v.z = speedZ;
			rb.velocity = v;
            print("velocity: " + rb.velocity);
		}

	}

	public void move(float Vx, float Vy, float Vz)
	{
		rb.velocity = new Vector3 (Vx, Vy, Vz);
	}

	public bool conditionToJump(float Vz, float firingAngle) {
		float TS = Mathf.Sqrt ((16 * rb.mass * gravity) / (pi * 1000));
		float y = 8 * rb.mass * Mathf.Tan (firingAngle * Mathf.Deg2Rad) * Mathf.Tan (firingAngle * Mathf.Deg2Rad);
		float z = pi * 1000 * Mathf.Sin (firingAngle * Mathf.Deg2Rad);

		float MS = Mathf.Sqrt (1 - y / z);

		float result = 40 * TS / MS;
		Debug.Log ("RESULT: " + result + " - VZ: "  +Vz);
		if (Vz > result){
			return true;
		}else {
			return false;
		}
	}

	public float speedZDown(float speedPre) {
		float downConstant = -1.4f * 0.13f * gravity * 2f;
		float speedAfter = Mathf.Sqrt (downConstant + speedPre * speedPre);
		return speedAfter;
	}

	public float speedYDown(float speedZAfter, float firingAngle) {
		return speedZAfter * Mathf.Tan (firingAngle * Mathf.Deg2Rad);
	}

	void OnTriggerEnter(Collider other){
		print ("Ten Collider Va cham: " + other.gameObject.name);
        if (other.gameObject.name == "Water Plane")

        {
			if (conditionToJump (rb.velocity.z, 10f) == true) {
				Debug.Log (other.transform.position);
				speedZ = speedZDown (rb.velocity.z);
				speedY = speedYDown (speedZ, 10f);
				isFalling = false;
			} else {
				isFalling = true;
			}
        }
        else if (other.gameObject.name == "In Front Of Lake"
            || other.gameObject.name == "Left Lake"
            || other.gameObject.name == "Right Lake")
        {
            isFalling = false;
            speedZ = -speedZDown(rb.velocity.z);
            speedY = speedYDown(speedZ, 10f);
        }
	}
		
}
