﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rpc : MonoBehaviour {

    public string connectionIP = "127.0.0.1";
    public int portNumber = 8895;
    private bool connected = false;

    //Khi server đc khởi tạo
    private void OnServerInitialized()
    {
        connected = true;
    }

    //ngat ket noi voi Server
    private void OnDisconnectedFromServer()
    {
        connected = false;
    }

    private void OnGUI()
    {


        if (!connected)
        {
            connectionIP = GUILayout.TextField(connectionIP);
            int.TryParse(GUILayout.TextField(portNumber.ToString()), out portNumber);

            if (GUILayout.Button("Connect"))
                Network.Connect(connectionIP, portNumber);
            if (GUILayout.Button("Host"))
            {
                Network.InitializeServer(4, portNumber, true);
            }
        }
        else
        {
            GUILayout.Label("Connections: " + Network.connections.Length.ToString());
        }

    }
}
