﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddLake : MonoBehaviour {

	// Use this for initialization
	public GameObject lake;
	void Start () {
		Instantiate(lake,new Vector3(170,160,144),Quaternion.identity);
		Instantiate(lake,new Vector3(240,160,144),Quaternion.identity);
		Instantiate (lake, new Vector3 (310,160,144), Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
