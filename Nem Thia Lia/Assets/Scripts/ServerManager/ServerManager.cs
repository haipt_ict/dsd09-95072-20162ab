﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ServerManager : MonoBehaviour {
    private int portNumber = 8895;
	private List<string[]> clientInfo = new List<string[]>();
	public Canvas nameCanvas;
	public Text text;
	public GameObject btn;
	public GameObject lake;

    // Use this for initialization
    void Start () {
        Network.InitializeServer(4, portNumber, true);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //khi client kết nối đến 
    private void OnConnectedToServer()
    {
    }

    //Khi server đc khởi tạo
    private void OnServerInitialized()
    {
        
    }

    //ngat ket noi voi Server
    private void OnDisconnectedFromServer()
    {
        
    }

	public void openCanvas(){
		nameCanvas.gameObject.SetActive (true);
		for (int i = 0; i < clientInfo.Count; i++) {
			text.text = clientInfo [i] [0];
			text.transform.position = new Vector3 (text.transform.position.x,text.transform.position.y + 5f,text.transform.position.z);
			btn.transform.position = new Vector3 (btn.transform.position.x,btn.transform.position.y + 5f,btn.transform.position.z);
		}
	}

	public void manageLake(){
		if (Network.connections.Length == 1) {
			SceneManager.LoadScene ("QLAo",LoadSceneMode.Single);
			//Instantiate(lake,new Vector3(0,8,0),Quaternion.identity);
		} else {
			//SceneManager.LoadScene ("QLAo");
			//Instantiate(lake,new Vector3(0,8,0),Quaternion.identity);
		}
	}

	public void disconnect(){
		int id = int.Parse (text.text);
		Network.CloseConnection (Network.connections [id - 1], true);
	}

    public void mangerNumberMember()
    {
		openCanvas();
		//foreach(string[] s in clientInfo){
		//	for (int i = 0; i < s.Length; i++) {
		//		print (s [i]);
		//		print ("___");
		//	}
		// }
    }
		
	[RPC]
	public void RpcServer(string s){
	}

	[RPC]
	public void sendInfoToServer(string id,string os,int width,int height){
		string w = width.ToString ();
		string h = height.ToString ();
		clientInfo.Add (new string[]{id,os,w,h});
	} 	
}
